/** 
 * How to run:
 *  node hello.js
 */


const myGreeting = require('./helloProperties.js');

function sayHello() {
    console.log("Hello, wonderful world!");
    console.log(myGreeting.greeting);
    console.log("This is " + myGreeting.whoAmI());
}

sayHello();

